# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mkinitfs
pkgver=3.8.0
# shellcheck disable=SC2034 # used for git versions, keep around for next time
_ver=${pkgver%_git*}
pkgrel=3
pkgdesc="Tool to generate initramfs images for Alpine"
url="https://gitlab.alpinelinux.org/alpine/mkinitfs"
arch="all"
license="GPL-2.0-only"
# currently we do not ship any testsuite
options="!check"
makedepends_host="busybox kmod-dev util-linux-dev cryptsetup-dev linux-headers"
makedepends="$makedepends_host"
depends="
	apk-tools>=2.9.1
	busybox-binsh
	busybox>=1.28.2-r1
	kmod
	lddtree>=1.25
	mdev-conf
	"
subpackages="$pkgname-doc"
install="$pkgname.pre-upgrade $pkgname.post-install $pkgname.post-upgrade"
triggers="$pkgname.trigger=/usr/share/kernel/*"
source="https://gitlab.alpinelinux.org/alpine/mkinitfs/-/archive/$pkgver/mkinitfs-$pkgver.tar.gz
	"

provides="initramfs-generator"
provider_priority=900 # highest

build() {
	make VERSION=$pkgver-r$pkgrel
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="
0f9dc966dacc125c0dd15ec7f23c4204c8a10f2d8d6d38a867ad95e40de76f2622a49f5540f8cd510be538cc36c8aaafffd0f424b0e1320050a9d96775afe98c  mkinitfs-3.8.0.tar.gz
"
