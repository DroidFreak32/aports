# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=nghttp2
pkgver=1.53.0
pkgrel=0
pkgdesc="Experimental HTTP/2 client, server and proxy"
url="https://nghttp2.org"
arch="all"
license="MIT"
makedepends_host="libev-dev openssl-dev>3 zlib-dev c-ares-dev"
checkdepends="cunit-dev"
subpackages="$pkgname-static $pkgname-doc $pkgname-dev $pkgname-libs"
source="https://github.com/nghttp2/nghttp2/releases/download/v$pkgver/nghttp2-$pkgver.tar.xz"

# temp allow textrels on riscv64
[ "$CARCH" = "riscv64" ] && options="$options textrels"

# secfixes:
#   1.41.0-r0:
#     - CVE-2020-11080
#   1.39.2-r0:
#     - CVE-2019-9511
#     - CVE-2019-9513

check() {
	make check
	# integration tests require go, which is only available
	# in community at the moment of writing. Disabling until
	# go is moved into main
#	cd "integration-tests"
#	make itprep
#	make it
}

build() {
	if [ -z "$BOOTSTRAP" ]; then
		# fat to keep static non-lto too for non-gcc
		# -20% size
		export CFLAGS="$CFLAGS -ffat-lto-objects -flto=auto"
		export CXXFLAGS="$CXXFLAGS -ffat-lto-objects -flto=auto"
	fi
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-examples \
		--disable-http3 \
		--disable-werror \
		--enable-app \
		--enable-static \
		--enable-threads \
		--with-libcares \
		--with-libev \
		--with-openssl \
		--without-jansson \
		--without-jemalloc \
		--without-libbpf \
		--without-libnghttp3 \
		--without-libngtcp2 \
		--without-libxml2 \
		--without-mruby \
		--without-neverbleed \
		--without-systemd \
		"$(want_check && echo --with-cunit || echo --without-cunit)"
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
ae3c7b12fc019fa139e94c16f69070734d9c600c1ebefad46758cd7f9a93d39f1d72fa2a9bce10e757141749436858316752157d9957e6aaa455764e2f667a4e  nghttp2-1.53.0.tar.xz
"
