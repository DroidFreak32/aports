# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Keith Maxwell <keith.maxwell@gmail.com>
pkgname=py3-ruamel.yaml
_pyname=ruamel.yaml
pkgver=0.17.26
pkgrel=0
pkgdesc="A YAML parser/emitter"
options="!check" # tests are not included in the latest artifact on PyPI
url="https://sourceforge.net/projects/ruamel-yaml/"
arch="noarch"
license="MIT"
replaces="py-ruamel py3-ruamel"
provides="py-ruamel=$pkgver-r$pkgrel py3-ruamel=$pkgver-r$pkgrel"
depends="py3-ruamel.yaml.clib"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest py3-ruamel.std.pathlib"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/r/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"
options="!check" # pypy no tests

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH="$PWD/build/lib" pytest-3 \
		--deselect=_test/test_issues.py::TestIssues::test_issue_82 \
		--deselect=_test/test_issues.py::TestIssues::test_issue_220 \
		--deselect=_test/test_issues.py::TestIssues::test_issue_238 \
		--deselect=_test/test_yamlobject.py::test_monster \
		--deselect=_test/test_yamlobject.py::test_qualified_name00 \
		_test/test_*.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
10856cba951b6b9356514ffd534e657b44f3445427c4fe097f9170f95224e76da5965e181dfea2e58b84279e80575e91d3b1e046dfc40de37189ad9bf685d702  ruamel.yaml-0.17.26.tar.gz
"
