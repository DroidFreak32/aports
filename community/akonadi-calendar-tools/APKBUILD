# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-calendar-tools
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by akonadi-calendar -> kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="CLI tools to manage akonadi calendars"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-calendar-dev
	akonadi-dev>=$pkgver
	calendarsupport-dev
	extra-cmake-modules
	kcalendarcore-dev
	kcalutils-dev
	kdoctools-dev
	libkdepim-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-calendar-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1fdc94e59fa45ecc25c1a7455966bb2506a5ebf7c9311a3d399920f7526d60e44d3d83d55cf5ba52555fee58919b1188c2a4a868b83432224e464e5120b03381  akonadi-calendar-tools-23.04.1.tar.xz
"
