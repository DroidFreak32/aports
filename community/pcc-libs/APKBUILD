# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=pcc-libs
pkgver=20230419
pkgrel=0
pkgdesc="The portable C compiler support libraries."
url="http://pcc.ludd.ltu.se/"
arch="x86 x86_64"
license="BSD"
subpackages="$pkgname-dev"
source="ftp://pcc.ludd.ltu.se/pub/pcc-libs/pcc-libs-$pkgver.tgz
	musl-fixes.patch
	fix-cflags.patch"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
b193e32d10cb7a7ce33e00c33842a6a3bc35f385422e87e5bd1975324672897ec6d492c496f83a43e980d9e469ca2b6a556826ea1fe965152f394d2a66d0bd89  pcc-libs-20230419.tgz
fd8c71cd583c6be29553f2b7e6ce66073afd6d0406ae86f6fbb6a36efe8685be4732d2989180a9b31af734cc9a0973c1c731472f02bad076121f6c3c58391fc4  musl-fixes.patch
2e2daa89350e9160fd7fe7f98189eebedb62be0eef712d9cbb8fdc2d7d2d47eece0d2d9756bca00fb44784c1067616fd114bed70ff6beb297db8da9d35fcb65b  fix-cflags.patch
"
