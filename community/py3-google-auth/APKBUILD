# Contributor: Keith Maxwell <keith.maxwell@gmail.com>
# Maintainer: Keith Maxwell <keith.maxwell@gmail.com>
pkgname=py3-google-auth
_pyname=google-auth
pkgver=2.18.0
pkgrel=1
pkgdesc="Google authentication library for Python."
url="https://google-auth.readthedocs.io/en/latest/"
arch="noarch"
license="Apache-2.0"
depends="
	python3
	py3-cachetools
	py3-asn1-modules
	py3-rsa
	py3-six
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-certifi
	py3-cryptography
	py3-flask
	py3-freezegun
	py3-grpcio
	py3-mock
	py3-oauth2client
	py3-openssl
	py3-pytest
	py3-pytest-cov
	py3-pytest-localserver
	py3-pyu2f
	py3-requests
	py3-responses
	py3-urllib3
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/g/google-auth/google-auth-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
2443c7948cb71803197dba2d57ff92f4c17981fa47ac383e9636d2ed239a7d71f43a4d8f61c7c90c651b8d13a0cfc5633a815d72bd731e92d83f3bbaa1b95118  google-auth-2.18.0.tar.gz
"
