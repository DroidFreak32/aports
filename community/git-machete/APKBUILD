# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=git-machete
pkgver=3.17.3
pkgrel=0
pkgdesc="git repository organizer & rebase/merge workflow automation tool"
url="https://github.com/VirtusLab/git-machete"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-mock"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/VirtusLab/git-machete/archive/refs/tags/v$pkgver/git-machete-$pkgver.tar.gz
	"

build() {
	python3 setup.py build
}

check() {
	pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm0644 "$builddir"/completion/git-machete.completion.bash \
		"$pkgdir/usr/share/bash-completion/completions/git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.completion.zsh \
		"$pkgdir/usr/share/zsh/site-functions/_git-machete"
	install -Dm0644 "$builddir"/completion/git-machete.fish \
		-t "$pkgdir/usr/share/fish/completions"
}

sha512sums="
bc138636e96f2fb415c28e6a429cc5a10de7b1edd22f752ce1f552fa33f16f668c5bc6917186f437ccf28fc2be15c8dc9cf21c3c9ec4ab49e5d63af5fee0f521  git-machete-3.17.3.tar.gz
"
