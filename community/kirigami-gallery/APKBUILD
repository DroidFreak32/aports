# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kirigami-gallery
pkgver=23.04.1
pkgrel=0
arch="all !armhf" # armhf blocked by kirigami2 -> qt5-qtdeclarative
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
pkgdesc="Gallery application built using Kirigami"
license="LGPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kirigami2-dev
	kitemmodels-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
566e13fb0ead6222c7c7fca25830854da3122d4cd9929e5d10c5a196fcf05a253a1ef3754a4f9e4b6f03db1aa570d7504bcca7d025d708277fa0bbe9710a82de  kirigami-gallery-23.04.1.tar.xz
"
