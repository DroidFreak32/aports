# Contributor: Saijin-Naib <Saijin-Naib_package-maintenance@outlook.com>
# Maintainer: Saijin-Naib <Saijin-Naib_package-maintenance@outlook.com>
pkgname=libopenraw
pkgver=0.3.5
pkgrel=0
pkgdesc="Desktop agnostic effort to support digital camera RAW files"
url="https://libopenraw.freedesktop.org/"
arch="all"
license="LGPL-3.0-or-later"
makedepends="
	boost-dev
	cargo
	curl-dev
	gdk-pixbuf-dev
	libjpeg-turbo-dev
	libxml2-dev
	"
subpackages="$pkgname-dev $pkgname-pixbuf-loader"
source="https://libopenraw.freedesktop.org/download/libopenraw-$pkgver.tar.bz2"
options="net" # cargo fetch

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make install DESTDIR="$pkgdir"
}

loader() {
	pkgdesc="$pkgdesc (pixbuf loader)"
	install_if="$pkgname=$pkgver-r$pkgrel gdk-pixbuf-loaders"

	amove \
		usr/lib/gdk-pixbuf-*/*/loaders
}

sha512sums="
0cfe06616fafd1144d446640df108fe396e95ef690da2a127e374728f5f1ff3875f2840ecdee254dd36fbd8994f798e913f6ead2d89a9bdf77be7d2ba3ffb2ed  libopenraw-0.3.5.tar.bz2
"
