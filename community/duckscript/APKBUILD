# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=duckscript
pkgver=0.8.18
pkgrel=0
pkgdesc="Simple, extendable and embeddable scripting language"
url="https://sagiegurari.github.io/duckscript/"
arch="all"
license="Apache-2.0"
makedepends="
	bzip2-dev
	cargo
	openssl-dev
	zlib-dev
	"
source="https://github.com/sagiegurari/duckscript/archive/$pkgver/duckscript-$pkgver.tar.gz
	zip-features.patch
	use-zlib.patch
	"

case "$CARCH" in
	# fails on builders for some reason with new networking
	arm*|aarch64) options="$options !check" ;;
	riscv64) options="$options textrels" ;;
esac

_cargo_opts="--frozen --no-default-features --features tls-native"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	install -D -m755 target/release/duck -t "$pkgdir"/usr/bin/
}

sha512sums="
49e5134968834e8914e6177b583855fd0d40d31a0bf86ac52ed7c13fd14754d4e6de795010321f922a7a98520df106744d954eb8dae5e9026a16b894c0d2ef48  duckscript-0.8.18.tar.gz
d8cd5928c3143d7e77fe418c761f63807cebe941fbcaed6ee721452591160d9fd060a7527210f9de707b3682d189c56e220d5efb33615d4e758cd66435348a4f  zip-features.patch
38c67db9dd408f325cb2a29ed470d7ea89a62421c2ce1ecfdeeb496a6ea84dc8242508373a6d0f696db0afa350e58b3826ae80bc659ac3260320be71d5762789  use-zlib.patch
"
