# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kopeninghours
pkgver=23.04.1
pkgrel=0
pkgdesc="Library for parsing and evaluating OSM opening hours expressions"
url="https://invent.kde.org/libraries/kopeninghours"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND CC0-1.0 AND LGPL-2.0-or-later"
makedepends="
	bison
	doxygen
	extra-cmake-modules
	flex
	graphviz
	kholidays-dev
	ki18n-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopeninghours-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# evaluatetest and iterationtest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(evaluate|iteration)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
02cd56c313510a6fd44b7d3feeca9ef2a56004bc30cab555c5c388afb0176de52176e4bccdb718388ee05773132dd05e30b54cf770f7c4a7df4318782eddee67  kopeninghours-23.04.1.tar.xz
"
