# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.0.267
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/charliermarsh/ruff"
# ppc64le, s390x, riscv64: ring
# x86, armhf, armv7: fails tests on 32-bit
arch="all !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
makedepends="maturin cargo py3-installer"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/charliermarsh/ruff/archive/v$pkgver/ruff-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	# shadow git repo for tests
	git init

	cargo fetch --locked
}

build() {
	maturin build --release --frozen --manylinux off

	./target/release/ruff --generate-shell-completion bash > $pkgname.bash
	./target/release/ruff --generate-shell-completion fish > $pkgname.fish
	./target/release/ruff --generate-shell-completion zsh > $pkgname.zsh
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		target/wheels/*.whl

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
d86103eadc454fc49169f63cac53c65946c6276f8fe3fb56f81ea31e3e9721e09c710ed35baf1470b1b90306b2ca1606cac8d92e3b1d4d4d3ca445ce215aa215  ruff-0.0.267.tar.gz
"
